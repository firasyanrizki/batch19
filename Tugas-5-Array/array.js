console.log('   No.1    ')
var enter = '                '

function range (startNum, finishNum) {
    var rangeArr = [];

    if (startNum > finishNum){
        var rangeLength = startNum - finishNum + 1;
        for (var i = 0; i< rangeLength; i++){
            rangeArr.push(startNum - i)
        }
    }else if (startNum < finishNum){
        var rangeLength = finishNum - startNum + 1;
        for (var i = 0; i < rangeLength; i++){
            rangeArr.push(startNum + i)
        }
    }else if (!startNum || !finishNum){
        return -1
    }
    return rangeArr
}
console.log("==========")
console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())
console.log(enter)

console.log('   No.2    ')
var enter = '                '

    function rangeWithStep (startNum, finishNum, step){
        var rangeArr = [];

        if (startNum > finishNum){
            var currentNum = startNum;
            for (var i = 0; currentNum >= finishNum; i++){
                rangeArr.push(currentNum)
                currentNum -= step
            }
        }else if (startNum < finishNum){
            var currentNum = startNum;
            for (var i = 0; currentNum <= finishNum; i++){
                rangeArr.push(currentNum)
                currentNum += step
            }
        }else if (!startNum || !finishNum || step){
            return -1
        }

        return rangeArr
    }
console.log("==========")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]
console.log(enter)

console.log('   No.3    ')
var enter = '                '

function sum(startNum, finishNum, step){
    var rangeArr = [];
    var distance;

    if(!step){
        distance=1
    }else {
        distance = step
    }
    if (startNum > finishNum){
        var currentNum = startNum;
        for (var i = 0; currentNum >= finishNum; i++){
            rangeArr.push(currentNum)
            currentNum -= distance
        }
    } else if (startNum < finishNum){
        var currentNum = startNum;
        for (var i = 0; currentNum <= finishNum; i++){
            rangeArr.push(currentNum)
            currentNum += distance
        }
    } else if (!startNum && !finishNum && !step){
        return 0
        
    } else if (startNum){
        return startNum
    }

    var total = 0;
    for (var i = 0; i < rangeArr.length; i++){
        total = total + rangeArr[i]
    }
    return total
}
console.log("==========")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log(enter)

console.log('   No.4    ')
var enter = '                '

var input1 = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
] 

var input2 = [
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"]
]

var input3 = [
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"]
]

var input4 = [
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

var enter = '                '

function dataHandling (data){
    var dataLength = data.length
    for (var i=0; i<dataLength; i++){
        var id = "Nomor ID :" + data[i][0] 
        var nama = "Nama Lengkap :" + data[i][1] 
        var ttl = "TTL :" + data[i][2]+ " " + data[i][3] 
        var hobi = "Hobi :" + data[i][4]

        console.log(id)
        console.log(nama)
        console.log(ttl)
        console.log(hobi)

    }
}
console.log("==========")
dataHandling(input1)
console.log(enter)
dataHandling(input2)
console.log(enter)
dataHandling(input3)
console.log(enter)
dataHandling(input4)
console.log(enter)

console.log('   No.5    ')
var enter = '                '


console.log("==========")
