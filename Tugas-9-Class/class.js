class Animal {
    constructor(name){
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }
    get name (){
        return this._name
    }
    get legs(){
        return this._legs
    }
    set legs(amount){
        return this._legs = amount
    }
    get cold_blooded(){
        return this._cold_blooded
    }
}
var sheep = new Animal ("shaun");

console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)


//=======================

class Ape extends Animal{
    constructor(name, amount){
        super(name)
        this._legs = amount
    }
    yell(){
        console.log("Auooo")
    }
}
class Frog extends Animal {
    constructor(name){
        super(name)
    }
    jump(){
        console.log("hop hop")
    }
}
var sungokong = new Ape ("Kera Sakti", 2)
sungokong.yell()
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)

var kodok = new Frog ("Buduk")
kodok.jump()
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)