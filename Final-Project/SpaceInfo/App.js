import React, {useState, useEffect} from 'react';
import { View, Text, Button, Image, ActivityIndicator } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

import { createDrawerNavigator } from '@react-navigation/drawer';
import MainTabScreen from './screens/MainTabScreen';

import { DrawerContent } from './screens/DrawerContent';

import {AuthContext} from './components/context';

import RootStackScreen from './screens/RootStackScreen';

const Drawer = createDrawerNavigator();

const App =  () => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [userToken, setUserToken] = React.useState(null);

  const authContext = React.useMemo(() => ({
    signIn: () => {
      setUserToken('123');
      setIsLoading(false);
    },
    signOut: () => {
      setUserToken(null);
      setIsLoading(false);
    },
    signUp: () => {
      setUserToken('123');
      setIsLoading(false);
    },
  }));

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  }, []);

  if(isLoading) {
    return(
      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <ActivityIndicator size='large'/>
      </View>
    );
  }

  return (
    <AuthContext.Provider value={authContext}>
    <NavigationContainer>
      { userToken !== null ? (
        <Drawer.Navigator drawerContent={props => <DrawerContent {...props}/>}>
        <Drawer.Screen name="homeDrawer" component={MainTabScreen} />
        {/*<Drawer.Screen name="thankyou" component={ThankyouScreen} />
        <Drawer.Screen name="profile" component={ProfileStackScreen} />*/}
      </Drawer.Navigator>

      )
    :
      <RootStackScreen/>
    }
      
      
    </NavigationContainer>
    </AuthContext.Provider>
  );
};

export default App;


{/* <Stack.Navigator screenOptions={{
        headerStyle: {
          backgroundColor: '#60B8CB',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold'
        }
      }}>
        <Stack.Screen name="login" component={LoginScreen} options={{
          title:''
        }}/>
        <Stack.Screen name="home" component={HomeScreen} />
        <Stack.Screen name="details" component={DetailsScreen} />
        <Stack.Screen name="profile" component={ProfileScreen} />
        <Stack.Screen name="signup" component={SignupScreen} />
        <Stack.Screen name="thankyou" component={ThankyouScreen} options={{
          title:''
        }}/>
      </Stack.Navigator> */}