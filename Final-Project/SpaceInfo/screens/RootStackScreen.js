import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import SplashScreen from './SplashScreen';
import LoginScreen from './LoginScreen';
import SignupScreen from './SignupScreen';
import ThankyouScreen from './ThankyouScreen';


const RootStack = createStackNavigator();

const RootStackScreen = ({navigation}) => (
    <RootStack.Navigator headerMode='none'>
        <RootStack.Screen name='SplashScreen' component={SplashScreen}/>
        <RootStack.Screen name='LoginScreen' component={LoginScreen}/>
        <RootStack.Screen name='SignupScreen' component={SignupScreen}/>
        <RootStack.Screen name='thankyou' component={ThankyouScreen}/>
    </RootStack.Navigator>
);

export default RootStackScreen;