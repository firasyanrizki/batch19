import React from 'react';
import {
   View, 
   Text, 
   Button, 
   StyleSheet, 
   StatusBar, 
   Image,
   TouchableOpacity,
   Dimensions
   } from 'react-native';

import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const ThankyouScreen = () => {
    return (
      <View style={styles.container}>
      <View>
        <View style={styles.header}>
            <Animatable.Image
                animation='slideInUp'
                duration='3000'
            source={require('../assets/rocket.png')}
            />
        </View>
        <View style={{
            flex: 1,
            height: "100%",
            paddingVertical: "120%",
            paddingHorizontal: -150  
          }}>
          <Text style={styles.title}>                         Thankyou</Text>
          <Text style={styles.title}>          Please Check Your Email</Text>
          <Image 
            source={require('../assets/earth.png')}
            />
        </View>
      </View>
      </View>
    )
  }

  export default ThankyouScreen;

  const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: '#007AFF'
    },
    header: {
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center'
      },
      title: {
          color: '#fff',
          fontSize: 25,
          fontWeight: 'bold',
          alignItems: 'center',
          justifyContent: 'center',
      },
      text: {
          color: 'grey',
          marginTop:5
      },
      button: {
          alignItems: 'flex-end',
          marginTop: 20
      }
  });
    
