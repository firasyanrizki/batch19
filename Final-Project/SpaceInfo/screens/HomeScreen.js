import React from 'react';
import {
   View, 
   Text, 
   Button, 
   StyleSheet, 
   StatusBar, 
   Image,
   TouchableOpacity,
   Platform,
   ScrollView
   } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';

const HomeScreen = () => {
    return (
    <ScrollView>
        <View>
        <View style={styles.header}>
            <Animatable.Image
                animation='zoomIn'
                //duration='-1500'
            source={require('../assets/spacex.png')}
            />
            </View>
            <View style={styles.text}> 
        <Text style={styles.text}>
            SpaceX
        </Text>
        </View>  
        </View>
        <Text style={styles.textSign}>  Space Exploration Technologies Corp. (SpaceX) is an American aerospace manufacturer and space transportation services company headquartered in Hawthorne, California. It was founded in 2002 by Elon Musk with the goal of reducing space transportation costs to enable the colonization of Mars. SpaceX has developed several launch vehicles, the Starlink satellite constellation, the Dragon cargo spacecraft, and flown humans to the International Space Station on the SpaceX Dragon 2.
        </Text>

        <View>
        <View style={styles.header}>
            <Animatable.Image
                animation='zoomIn'
                //duration='-1500'
            source={require('../assets/blueorigin.png')}
            />
            </View>
            <View style={styles.text}> 
        <Text style={styles.text}>
            Blue Origin
        </Text>
        </View>  
        </View>
        <Text style={styles.textSign}>  Blue Origin, LLC is an American privately funded aerospace manufacturer and sub-orbital spaceflight services company headquartered in Kent, Washington. Founded in 2000 by Jeff Bezos, the company is led by CEO Bob Smith and aims to make access to space cheaper and more reliable through reusable launch vehicles. Blue Origin is employing an incremental approach from suborbital to orbital flight, with each developmental step building on its prior work. The company motto is Gradatim Ferociter, Latin for "Step by Step, Ferociously".
        </Text>

        <View>
        <View style={styles.header}>
            <Animatable.Image
                animation='zoomIn'
                //duration='-1500'
            source={require('../assets/rocketlab.png')}
            />
            </View>
            <View style={styles.text}> 
        <Text style={styles.text}>
            Rocket Lab
        </Text>
        </View>  
        </View>
        <Text style={styles.textSign}>  Rocket Lab is a private American aerospace manufacturer and small satellite launch service provider with a wholly owned New Zealand subsidiary. It developed a sub-orbital sounding rocket named Ātea and currently operates a lightweight orbital rocket known as Electron, which provides dedicated launches for smallsats and CubeSats. The company was founded in New Zealand in 2006 by engineer Peter Beck and established headquarters in California in the United States in 2013.
        </Text>

        <View>
        <View style={styles.header}>
            <Animatable.Image
                animation='zoomIn'
                //duration='-1500'
            source={require('../assets/dragon.png')}
            />
            </View>
            <View style={styles.text}> 
        <Text style={styles.text}>
            SpaceX Dragon
        </Text>
        </View>  
        </View>
        <Text style={styles.textSign}>  The SpaceX Dragon , also known as Dragon 1 or Cargo Dragon , was a class of reusable cargo spacecraft developed by SpaceX , an American private space transportation company. Dragon was launched into orbit by the company's Falcon 9 launch vehicle to resupply the International Space Station (ISS). It is now superseded by SpaceX Dragon 2. During its maiden flight in December 2010, Dragon became the first commercially built and operated spacecraft to be recovered successfully from orbit. On 25 May 2012, a cargo variant of Dragon became the first commercial spacecraft to successfully rendezvous with and attach to the ISS. SpaceX is contracted to deliver cargo to the ISS under NASA 's Commercial Resupply Services program, and Dragon began regular cargo flights in October 2012. With the Dragon spacecraft and the Orbital ATK Cygnus , NASA seeks to increase its partnerships with domestic commercial aviation and aeronautics industry. 
        </Text>
    </ScrollView>
    );
  };

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#007AFF'
    },
    textSign: {
        fontSize: 14,
        fontWeight: '600',
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        alignItems: 'center',
        marginTop: 50
    },
    signIn: {
        width: '100%',
        height: 108,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    text: {
        fontSize: 18,
        fontWeight: "bold",
        alignItems: 'center',
        justifyContent: 'center',

    },
    header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 100
    }
});   



{/*<View style={{ flex:1, alignItems: 'center', justifyContent: 'center'}}>
            <Image source={require('../assets/Ellipse1.png')}/>
            <Text>Home Screen</Text>
            <Button
              title='Details'
            onPress={() => navigation.navigate("details")}
            />
            <Button
              title='Profile'
            onPress={() => navigation.navigate("profile")}
            />
            <Button
              title='logout'
              onPress={() => navigation.navigate('login')}
              />
            
        </View>
    );
};
*/}