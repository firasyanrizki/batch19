import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Dimensions,
    Image,
    StyleSheet,
    StatusBar
} from 'react-native';

import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const SplashScreen = ({navigation}) => {
    return (
        <View style={styles.container}>
            <StatusBar backgroundColor='#007AFF' barStyle='#light-content'/>
        <View style={styles.header}>
            <Animatable.Image
                animation='slideInUp'
                //duration='-1500'
            source={require('../assets/logo.png')}
            style={styles.logo}
            resizeMode='stretch'
            />
        </View>
        <View style={styles.footer}>
            <Text style={styles.title}>Welcome to SpaceInfo</Text>
            <Text style={styles.text}>Login with your account</Text>
            <View style={styles.button}/>
            <TouchableOpacity onPress={() =>navigation.navigate('LoginScreen')}>
                <LinearGradient
                    colors={['#08d4c4','#01ab9d']}
                    style={styles.signIn}
            >
                <Text style={styles.textSign}>Next</Text>
                <MaterialIcons
                    name='navigate-next'
                    color='#fff'
                    size={20}/>

                </LinearGradient>        
            </TouchableOpacity>
        </View>
        </View>
    );
};

export default SplashScreen;

const {height} = Dimensions.get("screen");
const height_logo = height * 0.28;

const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: '#007AFF'
    },
    header: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    footer: {
        flex: 1,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 50,
        paddingHorizontal: 30
    },
    logo: {
        width: height_logo,
        height: height_logo
    },
    title: {
        color: '#05375a',
        fontSize: 30,
        fontWeight: 'bold'
    },
    text: {
        color: 'grey',
        marginTop:5
    },
    button: {
        alignItems: 'flex-end',
        marginTop: 20
    },
    signIn: {
        width: 150,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        flexDirection: 'row'
    },
    textSign: {
        color: 'white',
        fontWeight: 'bold'
    }
  });
  