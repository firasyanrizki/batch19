import React from 'react';
import {
     View, 
     Text, 
     Button, 
     StyleSheet, 
     StatusBar,
     Image,
     Dimensions,
     TouchableOpacity
     } from 'react-native';
import * as Animatable from 'react-native-animatable';


const DetailScreen = () => {
    return (
        <View>
            <View style={styles.header}>
            <Animatable.Image
                animation='zoomIn'
                //duration='-1500'
            source={require('../assets/elon.png')}
            />
            </View>
            <View style={styles.title}>
                <Text style={{fontSize: 18}}>Elon Musk</Text>
            </View>
            <View style={styles.text}>
                <Text style={{fontSize: 13}}>CEO SpaceX</Text>
            </View>
            <View style={styles.header}>
            <Animatable.Image
                animation='zoomIn'
                //duration='-1500'
            source={require('../assets/peter.png')}
            />
            </View>
            <View style={styles.title}>
                <Text style={{fontSize: 18}}>Peter Beck</Text>
            </View>
            <View style={styles.text}>
                <Text style={{fontSize: 13}}>CEO Rocket Lab</Text>
            </View>
            <View style={styles.header}>
            <Animatable.Image
                animation='zoomIn'
                //duration='-1500'
            source={require('../assets/jeff.png')}
            />
            </View>
            <View style={styles.title}>
                <Text style={{fontSize: 18}}>Jeff Bezos</Text>
            </View>
            <View style={styles.text}>
                <Text style={{fontSize: 13}}>CEO Blue Origin</Text>
            </View>
            
        </View>
    );
};

export default DetailScreen;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#007AFF'
    },
    title: {
        color: '#05375a',
        fontSize: 100,
        fontWeight: 'bold',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: 'grey',
        marginTop:5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    header: {
    flex: 2,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 62

    }

  });
  