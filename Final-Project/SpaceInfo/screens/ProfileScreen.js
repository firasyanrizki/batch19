import React from 'react';
import {
     View, 
     Text, 
     Button, 
     StyleSheet, 
     StatusBar,
     Image,
     Dimensions,
     TouchableOpacity,
     ScrollView
     } from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';


const ProfileScreen = () => {
    return (
        <ScrollView>
            <View style={styles.header}>
            <Animatable.Image
                animation='zoomIn'
                //duration='-1500'
            source={require('../assets/kelompoknama.png')}
            />
            </View>
            <View style={styles.header}>
            <Animatable.Image
                animation='zoomIn'
                //duration='-1500'
            source={require('../assets/javascript.png')}
            />
            </View>
            <View style={styles.header}>
            <Animatable.Image
                animation='zoomIn'
                //duration='-1500'
            source={require('../assets/react.png')}
            />
            </View>
            <View style={styles.header}>
            <Animatable.Image
                animation='zoomIn'
                //duration='-1500'
            source={require('../assets/vscode.png')}
            />
            </View>
        </ScrollView>
    );
};

export default ProfileScreen;

const styles = StyleSheet.create({
    title: {
        color: '#05375a',
        fontSize: 100,
        fontWeight: 'bold',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: 'grey',
        marginTop:5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 100
    },
    tinylogo: {
        width: 400,
        height: 200,
        marginVertical: 1,
        alignContent: 'center',
        justifyContent: 'center'

    },

  });
  