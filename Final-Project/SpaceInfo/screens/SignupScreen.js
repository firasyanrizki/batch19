import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Dimensions,
    Image,
    Button,
    Platform,
    TextInput,
    StyleSheet,
    StatusBar
} from 'react-native';

import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/Ionicons';



const LoginScreen = ({navigation}) => {

    const [data, setData] = React.useState({
        email:'',
        password:'',
        confirm_password: '',
        check_textInputChange: false,
        secureTextEntry: true,
        confirm_secureTextEntry: true,
    });

    const textInputChange = (val) => {
        if(val.length != 0 ) {
            setData({
                ...data,
                email: val,
                check_textInputChange: true
            });
        } else {
            setData({
                ...data,
                email: val,
                check_textInputChange: false
            }); 
        }

    }

    const handlePasswordChange = (val) => {
        setData({
            ...data,
            password: val
        });
    }
    const handleConfirmPasswordChange = (val) => {
      setData({
          ...data,
          confirm_password: val
      });
  }

    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        })
    }

    const updateConfirmSecureTextEntry = () => {
      setData({
          ...data,
          confirm_secureTextEntry: !data.confirm_secureTextEntry
      })
  }

    return (
        <View style={styles.container}>
                <StatusBar backgroundColor='#007AFF' barStyle='#light-content'/>
            <View style={styles.header}>
                <Text style={styles.text_header}>Register Here !</Text>
            </View >
            <Animatable.View 
                animation='fadeInUpBig'
                style={styles.footer}
            >
                <Text style={styles.text_footer}>Email</Text>
                <View style={styles.action}>
                    <Icon
                        name='person-outline'
                        color='#05375a'
                        size={20} 
                />
                <TextInput
                    placeholder='example@spaceinfo.com'
                    style={styles.textInput}
                    autoCapitalize='none'
                    onChangeText={(val) => textInputChange(val)}
                />
                {data.check_textInputChange ?
                <Icon
                    name='ios-checkmark-done'
                    color='green'
                    size={20}
                />
                : null}
                </View>
                <Text style={[styles.text_footer, {
                    marginTop: 35
                }]}>Password</Text>
                <View style={styles.action}>
                    <Icon
                        name='ios-lock-closed-outline'
                        color='#05375a'
                        size={20} 
                />
                <TextInput
                    placeholder='***************'
                    secureTextEntry={data.secureTextEntry ? true : false}
                    style={styles.textInput}
                    autoCapitalize='none'
                    onChangeText={(val) => handlePasswordChange(val)}
                />

                <TouchableOpacity
                    onPress={updateSecureTextEntry}
                >
                    {data.secureTextEntry ? 
                <Icon
                    name='md-eye-off-outline'
                    color='grey'
                    size={20}
                />
                :
                <Icon
                    name='md-eye'
                    color='grey'
                    size={20}
                />
                }
                </TouchableOpacity>
                </View>

                <Text style={[styles.text_footer, {
                    marginTop: 35
                }]}>Confirm Password</Text>
                <View style={styles.action}>
                    <Icon
                        name='ios-lock-closed-outline'
                        color='#05375a'
                        size={20} 
                />
                <TextInput
                    placeholder='***************'
                    secureTextEntry={data.confirm_secureTextEntry ? true : false}
                    style={styles.textInput}
                    autoCapitalize='none'
                    onChangeText={(val) => handleConfirmPasswordChange(val)}
                />

                <TouchableOpacity
                    onPress={updateConfirmSecureTextEntry}
                >
                    {data.secureTextEntry ? 
                <Icon
                    name='md-eye-off-outline'
                    color='grey'
                    size={20}
                />
                :
                <Icon
                    name='md-eye'
                    color='grey'
                    size={20}
                />
                }
                </TouchableOpacity>
                </View>

                <View style={styles.button}>
                    <TouchableOpacity
                        style={styles.signIn}
                        onPress={() =>navigation.navigate('thankyou')}
                    >
                    <LinearGradient
                        colors={['#08d4c4', '#01ab9d']}
                        style={styles.signIn}
                    >
                        <Text style={[styles.textSign, {
                            color:'#fff'
                        }]}>Sign In</Text>
                    </LinearGradient>
                    </TouchableOpacity>        
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={[styles.signIn, {
                            borderColor: '#009387',
                            borderWidth: 1,
                            marginTop: 15 
                        }]}
                    >
                        <Text style={[styles.textSign, {
                            color: '#009387'
                        }]}>Log In</Text>

                    </TouchableOpacity>
                </View>

            </Animatable.View>    
        </View>
    );
};

export default LoginScreen;

const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: '#007AFF'
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50
    },
    footer: {
        flex: 3,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30
    },
    text_footer: {
        color: '#05375a',
        fontSize: 18
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -25,
        paddingLeft: 10,
        color: '#05375a',
        paddingVertical: 20
    },
    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
    },
    button: {
        alignItems: 'center',
        marginTop: 0
    },
    signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold'
        
    }
  });

