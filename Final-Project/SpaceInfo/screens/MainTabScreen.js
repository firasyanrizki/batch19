import * as React from 'react';

import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'; 
import { createStackNavigator } from '@react-navigation/stack';

import Icon from 'react-native-vector-icons/Ionicons';

import HomeScreen from './HomeScreen';
import DetailsScreen from './DetailsScreen';
import ProfileScreen from './ProfileScreen';


const HomeStack = createStackNavigator();
const DetailsStack = createStackNavigator();
const ProfileStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();

const MainTabScreen = () => (
<Tab.Navigator
      initialRouteName="home"
      activeColor="#fff"
    >
      <Tab.Screen
        name="home"
        component={HomeStackScreen}
        options={{
          tabBarLabel: 'home',
          tabBarColor: '#1f65ff',
          tabBarIcon: ({ color }) => (
            <Icon name="ios-home" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="details"
        component={DetailsStackScreen}
        options={{
          tabBarLabel: 'details',
          tabBarColor: '#1f65ff',
          tabBarIcon: ({ color }) => (
            <Icon name="ios-chevron-down-circle-outline" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="profile"
        component={ProfileStackScreen}
        options={{
          tabBarLabel: 'profile',
          tabBarColor: '#1f65ff',
          tabBarIcon: ({ color }) => (
            <Icon name="ios-person" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
);

export default MainTabScreen;

const HomeStackScreen = ({navigation}) => (
    <HomeStack.Navigator screenOptions={{
      headerStyle: {
        backgroundColor: '#007AFF',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold'
      }
    }}>
      <HomeStack.Screen name="home" component={HomeScreen} options={{
        headerLeft: () => (
          <Icon.Button name='ios-menu' size={25}
          backgroundColor='#007AFF' onPress={() => navigation.openDrawer()}></Icon.Button>
        )
      }}/>
    
    </HomeStack.Navigator>
  );

  const DetailsStackScreen = ({navigation}) => (
    <DetailsStack.Navigator screenOptions={{
      headerStyle: {
        backgroundColor: '#007AFF',
      },
      headerTintColor: '#fff', 
      headerTitleStyle: {
        fontWeight: 'bold'
      }
    }}>
      <DetailsStack.Screen name="details" component={DetailsScreen} options={{
        headerLeft: () => (
          <Icon.Button name='ios-menu' size={25}
          backgroundColor='#007AFF' onPress={() => navigation.openDrawer()}></Icon.Button>
        )
      }}/>
    
    </DetailsStack.Navigator>
  );

  const ProfileStackScreen = ({navigation}) => (
    <ProfileStack.Navigator screenOptions={{
      headerStyle: {
        backgroundColor: '#007AFF',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold'
      }
    }}>
      <ProfileStack.Screen name="profile" component={ProfileScreen} options={{
        headerLeft: () => (
          <Icon.Button name='ios-menu' size={25}
          backgroundColor='#007AFF' onPress={() => navigation.openDrawer()}></Icon.Button>
        )
      }}/>
    
    </ProfileStack.Navigator>
  );
  