console.log('   No.1    ')
var enter = '                '
console.log("Mengubah fungsi menjadi fungsi arrow")
const golden = () =>{
    console.log("this is golden")
}

golden();
console.log(enter)
console.log('   No.2    ')
var enter = '                '
console.log("Object literal di ES6")

const newFunction = (firstName, lastName) =>{
    return {
        firstName,lastName,
        fullName(){
            console.log(firstName + " " + lastName)
            return
        }
    }
}
newFunction("william", "imoh").fullName()
console.log(enter)

console.log('   No.3    ')
var enter = '                '
console.log("Destructuring")

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
const {firstName, lastName, destination, occupation, spell} = newObject

console.log (firstName, lastName, destination, occupation, spell)
console.log(enter)

console.log('   No.4    ')
var enter = '                '
console.log("Array Spreading")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]
console.log(combined)
console.log(enter)

console.log('   No.5    ')
var enter = '                '
console.log("Template Literals")

const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
'incididunt ut labore et dolore magna aliqua. Ut enim' +
' ad minim veniam'

console.log(before) 



