// Soal No. 1 (Membuat kalimat),
var tugas = 'N0 1'
var garispisah = '=========================='

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log(tugas)
console.log('JavaScript', 'is', 'awesome', 'and', 'I', 'love', 'it!')
console.log(garispisah)

// Soal No.2 Mengurai kalimat (Akses karakter dalam string),
var tugas2 = 'NO 2'
var garispisah2 = '=========================='

var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]; 
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9]; // lakukan sendiri 
var fourthWord = sentence[10] + sentence[11] + sentence[12]; // lakukan sendiri 
var fifthWord = sentence[13] + sentence[14] + sentence[15]; // lakukan sendiri 
var sixthWord = sentence[16] + sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21]; // lakukan sendiri 
var seventhWord = sentence[22] + sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27]; // lakukan sendiri 
var eighthWord = sentence[29] + sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38]; // lakukan sendiri 

console.log(tugas2)
console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)
console.log(garispisah2)

// Soal No. 3 Mengurai Kalimat (Substring)
var tugas3 = 'NO 3'
var garispisah3 = '=========================='

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 15); // do your own! 
var thirdWord2 = sentence2.substring(15, 18); // do your own! 
var fourthWord2 = sentence2.substring(18, 20); // do your own! 
var fifthWord2 = sentence2.substring(20, 25); // do your own! 

console.log(tugas3)
console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);
console.log(garispisah3)

// Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String
var tugas4 = 'NO 4'
var garispisah4 = '=========================='

var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14); // do your own! 
var thirdWord3 = sentence3.substring(15, 17); // do your own! 
var fourthWord3 = sentence3.substring(18, 20); // do your own! 
var fifthWord3 = sentence3.substring(21, 25); // do your own! 

var firstWordLength = exampleFirstWord3.length 
var firstsecondWord3 = secondWord3.length
var firstthirdWord3 = thirdWord3.length
var firstfourthWord3 = fourthWord3.length
var firstfifthWord3 = fifthWord3.length

console.log(tugas4)
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + firstsecondWord3); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + firstthirdWord3); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + firstfourthWord3); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + firstfifthWord3); 
console.log(garispisah4)

