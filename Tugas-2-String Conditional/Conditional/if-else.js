// Output untuk Input nama = '' dan peran = ''
var nama = ""
var peran = ""
var garis = '======================'
if (nama == "") {
    console.log("Nama harus diisi!")
} else if ( peran == "") {
    console.log("Peran harus diisi!")
}

console.log(garis)

//Output untuk Input nama = 'John' dan peran = ''
var nama = "John"
var peran = ""
var garis2 = '======================'
if (nama == "John") {
    console.log("Halo John, Pilih peranmu untuk memulai game!")
} else if ( peran == "") {
    console.log("Peran harus diisi!")
}

console.log(garis2)

//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
var nama = "Jane"
var peran = "Penyihir"
var garis3 = '======================'
if ( nama == "Jane" ) {
    console.log("Selamat datang di Dunia Warewolf, Jane")
    if(nama == "Jane" && peran == "Penyihir" ) {
        console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")    
    }}
console.log(garis3)

//Output untuk Input nama = 'Jenita' dan peran 'Guard'
var nama = "Jenita"
var peran = "Guard"
var garis4 = '======================'
if ( nama == "Jenita" ) {
    console.log("Selamat datang di Dunia Warewolf, Jenita")
    if(nama == "Jenita" && peran == "Guard" ) {
        console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")    
    }}
console.log(garis4)

//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
var nama = "Junaedi"
var peran = "Warewolf"
var garis5 = '======================'
if ( nama == "Junaedi" ) {
    console.log("Selamat datang di Dunia Warewolf, Junaedi")
    if(nama == "Junaedi" && peran == "Warewolf" ) {
        console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!" )    
    }}
console.log(garis5)

